import firebase from '@firebase/app'
import 'firebase/firestore';
import 'firebase/storage'

var config = {
    apiKey: "AIzaSyAsUsQnIZuEE_xEqJbmTFcF_9Yb6oEFZjw",
    authDomain: "novel-c4414.firebaseapp.com",
    databaseURL: "https://novel-c4414.firebaseio.com",
    projectId: "novel-c4414",
    storageBucket: "novel-c4414.appspot.com",
    messagingSenderId: "800056383592",
    appId: "1:800056383592:web:28388da44aa7a7cb8043dd",
    measurementId: "G-8XKK4QHW9H"
};

const fb = firebase.initializeApp(config);
export { fb }